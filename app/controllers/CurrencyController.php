<?php


namespace app\controllers;

use app\core\Controller;
use app\models\Currency;

class CurrencyController extends Controller
{

    public function actionIndex()
    {
        $ratesArr = Currency::getRates();
        $calcResult = Currency::calc();

        if (isset($_POST['culc_submit']) && !empty($_POST['sum'])){

            $sum = $_POST['sum'];
            $rateFrom = $ratesArr[$_POST['currency_from']];
            $rateTo = $ratesArr[$_POST['currency_to']];

            $calcResult = Currency::calc($sum, $rateFrom, $rateTo);

            Currency::historyRecord($sum, $_POST['currency_from'], $rateFrom, $_POST['currency_to'], $rateTo, $calcResult);
        }

        if (isset($_POST['currency_submit'])){

            foreach ($ratesArr as $currency => $rate){
                $_SESSION['active_currency'][$currency] = $_POST[$currency];
            }

         }else{

            foreach ($ratesArr as $currency => $rate){
                $_SESSION['active_currency'][$currency] = 1;
            }
        }

        if (isset($_POST['history_submit'])){

            if ($_POST['history_count'] == 'all'){

                $_SESSION['history_count']  = count($_SESSION['history']);

            }else{

                $_SESSION['history_count'] = $_POST['history_count'];
            }

        }elseif(!isset($_SESSION['history_count']) || $_SESSION['history_count']>10){

            $_SESSION['history_count'] = count($_SESSION['history']);

        }


        $data['result'] = $calcResult;

        $data['rates'] = $ratesArr;


        $this->view->generate('calculator/index.php', $data);
    }

}