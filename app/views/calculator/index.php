<div class="container">
    <div class="row">

        <div class="currency-switcher">

            <h2>
                Выбор валюты которая будет учавствовать в конвертации
            </h2>

            <form class="switcher-form" action="/" method="post">

                <?php foreach ($data['rates'] as $currency => $rate):?>

                <div class="switcher">
                    <label for="usd"><?=$currency?>(<?=$rate?>)</label>
                    <input type="hidden" name="<?=$currency?>" id="usd" value="0">
                    <input type="checkbox" name="<?=$currency?>" id="usd" value="1" <?php if ($_SESSION['active_currency'][$currency]) echo 'checked' ?>>
                </div>

                <?php endforeach ?>

                <div class="switcher">
                    <input class="submit" type="submit" name="currency_submit" value="Активировать">
                </div>


            </form>

        </div>

    </div>
</div>

<div class="container">
    <div class="row">

        <div class="calculator">

            <h2>
                Конвертер валют <i class="fas fa-exchange-alt"></i>
            </h2>

            <form class="calculator-form" action="/" method="post">

                <div class="col5">

                    <p>
                        Меняю
                    </p>

                    <div class="calc-from">

                        <div class="calc-sum">
                            <input class="calc-input" type="number" name="sum" step="1" value="<?=$sum = $_POST['sum'] ?? 1; ?>">
                        </div>

                        <div class="currency-type">
                            <select name="currency_from" id="currency">

                                <?php foreach ($data['rates'] as $currency => $rate):?>
                                    <?php if ($_SESSION['active_currency'][$currency]): ?>
                                        <option value="<?=$currency?>" <?php if(isset($_POST['currency_from']) & $_POST['currency_from'] == $currency) echo 'selected'; ?>><?=$currency?></option>
                                    <?php endif;?>
                                <?php endforeach ?>

                            </select>
                        </div>

                    </div>
                </div>

                <div class="col1">

                    <div class="exchange-icone">
                        <i class="fas fa-exchange-alt"></i>
                    </div>

                </div>

                <div class="col5">

                    <p>
                        Получаю
                    </p>
                    <div class="calc-to">

                        <div class="calc-sum">
                            <?=$data['result']?>

                        </div>

                        <div class="currency-type">

                            <select name="currency_to" id="currency">

                                <?php foreach ($data['rates'] as $currency => $rate):?>
                                    <?php if ($_SESSION['active_currency'][$currency]): ?>
                                        <option value="<?=$currency?>" <?php if(isset($_POST['currency_to']) & $_POST['currency_to'] == $currency) echo 'selected'; ?>><?=$currency?></option>
                                    <?php endif ?>
                                <?php endforeach ?>

                            </select>
                        </div>

                    </div>
                </div>

                <div class="col12">

                    <div class="calc-submit">
                        <input class="submit" type="submit" name="culc_submit" value="Рассчитать">
                    </div>

                </div>


        </div>

        </form>

    </div>

</div>
</div>

<div class="container">
    <div class="row">

        <div class="history">

            <h2>
                История конвертирования валюты
            </h2>

            <form class="switcher-form" action="/" method="post">

                <div class="switcher">
                    <input class="submit" type="submit" name="history_submit" value="Показать">
                </div>

                <div class="switcher">

                    <select name="history_count" id="currency">
                        <option value="0" <?php if ($_SESSION['history_count'] == 0) echo 'selected'?>>0</option>
                        <option value="3" <?php if ($_SESSION['history_count'] == 3) echo 'selected'?>>3</option>
                        <option value="5" <?php if ($_SESSION['history_count'] == 5) echo 'selected'?>>5</option>
                        <option value="10" <?php if ($_SESSION['history_count'] == 10) echo 'selected'?>>10</option>
                        <option value="all" <?php if ($_SESSION['history_count'] > 10) echo 'selected'?>>Все</option>
                    </select>
                </div>

            </form>

            <?php if (isset($_SESSION['history']) & !empty($_SESSION['history']) & $_SESSION['history_count'] > 0): ?>

                <div class="history-list">

                        <div class="history-row">

                            <div class="history-item">
                                №
                            </div>

                            <div class="history-item">
                                Меняю
                            </div>

                            <div class="history-item">
                                Получаю
                            </div>

                            <div class="history-item">
                                Дата
                            </div>

                        </div>

                </div>

                <?php foreach ($_SESSION['history'] as $count => $item):?>

                    <?php if ($_SESSION['history_count'] == $count) break ?>

                    <div class="history-list">

                        <div class="history-row">

                            <div class="history-item">
                                <?=$count+1?>
                            </div>

                            <div class="history-item">
                                <?=$item['sum']?> <?=$item['currency_from']?>
                            </div>

                            <div class="history-item">
                                <?=$item['result']?> <?=$item['currency_to']?>
                            </div>

                            <div class="history-item">
                                <?=$item['date']?>
                            </div>

                        </div>

                    </div>

                <?php endforeach ?>

            <?php endif ?>


        </div>

    </div>
</div>
