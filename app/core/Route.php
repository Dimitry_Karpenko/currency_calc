<?php


namespace app\core;


class Route
{
	public static function start()
	{
		$uri = $_SERVER['REQUEST_URI'];
		$controllerName = 'Currency';
		$actionName = 'index';

		$routes = explode('/', trim($uri, '/'));

		if (!empty($routes[0])) $controllerName = $routes[0];

		if (!empty($routes[1])) $actionName = $routes[1];

		$controllerName = '\\app\\controllers\\' . ucfirst($controllerName) . 'Controller';
		$actionName = 'action' . ucfirst($actionName);

		$controller = new $controllerName;
		$controller->$actionName();

	}
}