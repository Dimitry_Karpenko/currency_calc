<?php


namespace app\core;


class View
{
    protected $layout = 'layout/basic.php';

    public function generate($view, $data = null, $layout = null)
    {
        if ($layout){
            $this->layout = $layout;
        }

        include 'app/views/' . $this->layout;
    }

}