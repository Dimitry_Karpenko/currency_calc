<?php


namespace app\models;


class Currency
{

    public static function getRates()
    {
        $apiUri = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';

        $ratesData = json_decode(file_get_contents($apiUri), true);

        $ratesArr['UAH'] = 1;



        foreach ($ratesData as $value){

            if ($value['ccy'] == 'BTC') continue;

            $ratesArr[$value['ccy']] = $value['buy'];

        }

       return $ratesArr;
    }

    public static function calc($sum = 1, $rateFrom = 1, $rateTo = 1)
    {
        $result = round($sum * ($rateFrom/$rateTo),2);

        return $result;
    }

    public static function historyRecord($sum, $currency_from, $rateFrom, $currency_to, $rateTo, $calcResult)
    {
        $date = date("Y-m-d H:i:s");

        if (isset($_SESSION['history'])){
            $history = $_SESSION['history'];
        }else{
            $history = [];
        }

        $transaction['date'] = $date;
        $transaction['sum'] = $sum;
        $transaction['currency_from'] = $currency_from;
        $transaction['rate_from'] = $rateFrom;
        $transaction['currency_to'] = $currency_to;
        $transaction['rate_to'] = $rateTo;
        $transaction['result'] = $calcResult;

        $history[] = $transaction;

        $_SESSION['history'] = $history;
    }


}